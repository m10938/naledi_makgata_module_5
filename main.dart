import 'package:flutter/material.dart'; 
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'screens/session_form.dart';

Future main() async{
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: const FirebaseOptions(
      apiKey: "AIzaSyBKz4Nz9zYaWLMk7XCslS5SfaPDgKxSTxI",
  authDomain: "first-projectt-7762e.firebaseapp.com",
  projectId: "first-projectt-7762e",
  storageBucket: "first-projectt-7762e.appspot.com",
  messagingSenderId: "140555289588",
  appId: "1:140555289588:web:60f6357b77473d401a6fa1"
)
  );

runApp(const MyApp());

}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
  
      // ignore: prefer_const_constructors
     initialRoute: "/",
    routes: {
      "/":(context)  => SessionScreen(),
      }
    );
  }
}

