
import 'dart:core';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';


class Sessionlist extends StatefulWidget {
  const Sessionlist({ Key? key, }) : super(key: key);

  @override
  State<Sessionlist> createState() => _SessionlistState(); 
}

class _SessionlistState extends State<Sessionlist> {
  
  final Stream<QuerySnapshot> _myUserSession = 
  FirebaseFirestore.instance.collection('session').snapshots();

  @override
  Widget build(BuildContext context) {
  
    TextEditingController nameFieldcntroler = TextEditingController();
    TextEditingController phoneNumFieldcntroler = TextEditingController();
    TextEditingController locationFieldcntroler = TextEditingController();
   
    void _delete(docId){
    FirebaseFirestore.instance
    .collection("session")
    .doc(docId)
    .delete()
    .then((value) => print("deleted"));
    }

    void _update(data){   

      var collection = FirebaseFirestore.instance.collection("session");
      nameFieldcntroler.text = data["name"];
      phoneNumFieldcntroler.text = data["phoneNumber"];
      locationFieldcntroler.text = data["location"];

    showDialog(context: context, 
    builder: (_) => AlertDialog(
      title: Text("Update"),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TextField(
        controller:nameFieldcntroler,
        ),
        TextField(
        controller:phoneNumFieldcntroler,
        ),
        TextField(
        controller:locationFieldcntroler,
        ),
       
        TextButton(
          onPressed: (){
           collection.doc(data["doc_Id"])
           .update({
            "name": nameFieldcntroler.text,
            "phoneNumber": phoneNumFieldcntroler.text,
            "location": locationFieldcntroler.text,
           });
           Navigator.pop(context);
          },
         child: Text("Update")),
      ]),
    )
    );
    }

    return StreamBuilder(
    
      stream: _myUserSession,
      builder: (BuildContext context,
       AsyncSnapshot<QuerySnapshot<Object?>> snapshot){
        if (snapshot.hasError){
          return const Text("Something went wrong");
        }
          if(snapshot.connectionState == ConnectionState.waiting){
          return   const Center (child: CircularProgressIndicator());
        }
       if(snapshot.hasData){
        return Row(
          children: [
           Expanded(
            child: SizedBox(
            height: (MediaQuery.of(context).size.height),
            width: (MediaQuery.of(context).size.width),
            child: ListView(
              children: snapshot.data!.docs
              .map((DocumentSnapshot documentSnapshot)  {
                  Map<String, dynamic> data = documentSnapshot.data()! as Map<
                  String, dynamic>;
                  return Column(
                    children: [
                      Card(
                        child: Column(
                          children:[
                      ListTile(
                        title: Text(data['name']),
                        subtitle:Text(data['phoneNumber']), 
                        trailing:Text(data['location']),                        
                      ),
                      ButtonTheme(
                        child: ButtonBar(
                        children:[
                          OutlineButton.icon(
                            onPressed:(){                             
                              _update(data);
                            },
                            icon: Icon(Icons.edit),
                            label: Text("Edit"),
                            ),
                             OutlineButton.icon(
                            onPressed:(){
                             _delete(data["doc_Id"]);
                            },
                            icon: Icon(Icons.remove),
                            label: Text("Delete"),
                            )
                        ],
                        ),
                      ),
                          ],
                        )
                      )
                    ],
                  );
                })
                .toList()
                   
            ),
            )
            )
          ],
        );

       }else{
        return(Text("No data"));
       }
       },
      
    );
  }
}
