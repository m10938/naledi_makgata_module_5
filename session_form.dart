// ignore_for_file: non_constant_identifier_names

import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'session_list.dart';


// ignore: camel_case_types
class SessionScreen extends StatefulWidget {
  const SessionScreen({ Key? key,}) : super(key: key);
  
  @override
  State<SessionScreen> createState() => _SessionScreenState();
}

// ignore: camel_case_types
class _SessionScreenState extends State<SessionScreen> {
  @override
  Widget build(BuildContext context) {

     TextEditingController nameController = TextEditingController();
     TextEditingController phonenumberController = TextEditingController();
     TextEditingController locationController = TextEditingController();

    Future _addContacts(){
      final name = nameController.text;
      final phoneNumber = phonenumberController.text;
      final location = locationController.text;
    
       final ref = FirebaseFirestore.instance.collection("session").doc();
       
       return ref.set({"name":name,"phoneNumber":phoneNumber,"location":location,"doc_Id":ref.id})
       .then((value) => {
        nameController.text = "",
        phonenumberController.text = "",
       locationController.text = "",
       
       }
       )
      
       .catchError((onError) => log(onError));
       
    }
    return MaterialApp(
       // ignore: unnecessary_new
       theme: new ThemeData(
         primaryColor: Colors.blue
        
       ),
    
    home: Scaffold(
       appBar: AppBar(
       title: const Text("Student Medeting") 
     ),
      body: Center(
          child: ListView(
            children: [
              Column(
                children:<Widget> [
                   const SizedBox(height: 15.0,),
                  Padding(
                    padding:  const EdgeInsets.only(bottom:8.0),
                    child: TextFormField(
                      controller: nameController,
                      decoration: const InputDecoration(
                        labelText: "Enter Your name",
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue,width: 2.0)
                        )
                      ),
                    ),
                  ),
                   Padding(
                    padding:  const EdgeInsets.only(bottom:8.0),
                    child: TextFormField(
                       controller: phonenumberController,
                      decoration: const InputDecoration(
                        labelText: "Enter  Your Phone Number",
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue,width: 2.0)
                        )
                      ),
                     
                    ),
                  ),
                  Padding(
                    padding:  const EdgeInsets.only(bottom:8.0),
                    child: TextFormField(
                       controller: locationController,
                      decoration: const InputDecoration(
                        labelText: "Enter Your Location",
                        fillColor: Colors.white,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.blue,width: 2.0)
                        )
                      ),
                     
                    ),
                  ),
                  
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
        
                    ElevatedButton(                
                  onPressed: (){
                    _addContacts();             
                  },
                  child: const Text("Add Session"),
                     ),
                    ],
                  )
                ],
              ),
             const SizedBox(height: 10.0),
             Sessionlist(),
            
            ],
            
          ),
        
      ),
    ),
    );
  }
}
